package com.Maconsa.Controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.Maconsa.Entidad.Proveedor;
import com.Maconsa.Entidad.Usuario;
import com.Maconsa.Service.PersonaServiceImpl;
import com.Maconsa.Service.UsuarioServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UsuarioController {
	
	ObjectMapper Obj = new ObjectMapper();
	
	@Autowired
	private UsuarioServiceImpl usuarioServiceImpl;
	
	@Autowired
	private PersonaServiceImpl personaServiceImpl;
	
	 @RequestMapping(value= {"Usuario/Listar"},method=RequestMethod.GET)
		@ResponseBody 
		public String listarProveedores() throws JsonProcessingException {
			return  Obj.writeValueAsString(usuarioServiceImpl.listarUsuarios()); 
		}
	 
	 @RequestMapping(value= {"/Usuario/Guardar"},method=RequestMethod.POST)
	    @ResponseBody
	    public String crearProveedor(@RequestBody Usuario usuario) throws IOException {
		 usuarioServiceImpl.insertarUsuario(usuario);
	    	return  Obj.writeValueAsString("Usuario Ingresado");
	    }
		
		@RequestMapping(value= {"/Usuario/Eliminar{idUsuario}"},method=RequestMethod.GET)
	    @ResponseBody
	    public String eliminarProveedor(@PathVariable(value="idUsuario") int idUsuario) throws IOException {
			
			Usuario usuario= new Usuario();
			usuario.setIdUsuario(idUsuario);
			usuarioServiceImpl.eliminarUsuario(usuario);
	    	return  Obj.writeValueAsString("Usuario eliminado");
	    }
		
		@RequestMapping(value= "/Usuario/Buscar{descripcion}",method=RequestMethod.GET)
		@ResponseBody
		public String buscarProveedor(@PathVariable(value="descripcion") String descripcion, HttpServletRequest request ) throws JsonProcessingException
		{  
				ModelAndView modelAndView = new ModelAndView();

				int opcion=Integer.parseInt(request.getParameter("opcion"));
				if(opcion==1 && !descripcion.equals(""))
				{
					List<Usuario> listaUsuarios=usuarioServiceImpl.buscarUsuarioPorID("%"+descripcion+"%");
					return  Obj.writeValueAsString(listaUsuarios);
				}
				else if(opcion==2 && !descripcion.equals("")) {
					
					List<Usuario> listaUsuarios=usuarioServiceImpl.buscarUsuarioPorDescripcion("%"+descripcion+"%");
			        return  Obj.writeValueAsString(listaUsuarios);
				}else {
					return  Obj.writeValueAsString("");
				}
				
		}

}
