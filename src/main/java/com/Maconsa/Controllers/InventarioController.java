package com.Maconsa.Controllers;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.Maconsa.Entidad.Compra;
import com.Maconsa.Entidad.DetalleComprobanteDePago;
import com.Maconsa.Entidad.Producto;
import com.Maconsa.Entidad.Venta;
import com.Maconsa.Service.CompraServiceImpl;
import com.Maconsa.Service.InventarioServiceImpl;
import com.Maconsa.Service.ProductoServiceImpl;
import com.Maconsa.Service.VentaServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class InventarioController {

	
	private DecimalFormat df = new DecimalFormat("#.0");
	 private DecimalFormat df1 = new DecimalFormat("###,###.##");
	 
	@Autowired
	private InventarioServiceImpl inventarioServiceImpl;
	
	@Autowired
	private ProductoServiceImpl productoServiceImpl;
	
	@Autowired
	private  CompraServiceImpl compraServiceImpl;
	
	@Autowired
	private  VentaServiceImpl ventaServiceImpl;
	
	
	 ObjectMapper Obj = new ObjectMapper(); 
	
	@RequestMapping(value= {"/productosInventario"},method=RequestMethod.GET)
	public String listarProductos() throws JsonProcessingException {
		
        return  Obj.writeValueAsString(productoServiceImpl.findAll()); 
	}
	
	@RequestMapping(value= {"/totalProductos"},method=RequestMethod.GET)
	public String totalProductos() throws JsonProcessingException {
		
        return  Obj.writeValueAsString(productoServiceImpl.devolverTotalDeProductos()); 
	}
	
	@RequestMapping(value= {"/CostoInventario"},method=RequestMethod.GET)
	public String costoInventario() throws JsonProcessingException {

        return  Obj.writeValueAsString( df1.format(inventarioServiceImpl.devolverCostoInventario())); 
	}
	
	@RequestMapping(value="/listarComprasPeriodo", method = RequestMethod.GET,produces = "application/json")
	public String devolverComprasPeriodo(HttpServletRequest request) throws JsonProcessingException
	{
		int idProducto=Integer.parseInt(request.getParameter("idProducto"));
		int mes=Integer.parseInt(request.getParameter("mes"));
		int anio=Integer.parseInt(request.getParameter("anio"));
		List<DetalleComprobanteDePago>nuevoDetalle=null; 
		List<Compra>listaComprasPeriodo=new ArrayList<Compra>();
		for (int idCompra : inventarioServiceImpl.listaIdComprasPeriodo(anio,mes,idProducto)) {
			
			Compra compra=compraServiceImpl.verDetalleCompra(idCompra);
			for (DetalleComprobanteDePago detalle : compra.getComprobanteDePago().getDetalleComprobanteDePagos()) {
				if(detalle.getProducto().getIdProducto()==idProducto)
				{
					nuevoDetalle=new ArrayList<DetalleComprobanteDePago>();
					nuevoDetalle.add(detalle);
				}
			}
			compra.setUsuario(null);
			compra.getComprobanteDePago().setDetalleComprobanteDePagos(nuevoDetalle);
			listaComprasPeriodo.add(compra);
		}
		
		return  Obj.writeValueAsString(listaComprasPeriodo); 
	}
	
	@RequestMapping(value="/listarVentasPeriodo", method = RequestMethod.GET,produces = "application/json")
	public String devolverVentasPeriodo() throws JsonProcessingException
	{
		List<DetalleComprobanteDePago>nuevoDetalle=null;
		List<Venta>listaVentasPeriodo=new ArrayList<Venta>();
		for (int idVenta : inventarioServiceImpl.listaIdVentasPeriodo()) {
			
			Venta venta=ventaServiceImpl.verDetalleVenta(1);
			for (DetalleComprobanteDePago detalle : venta.getComprobanteDePago().getDetalleComprobanteDePagos()) {
				if(detalle.getProducto().getIdProducto()==1)
				{
					nuevoDetalle=new ArrayList<DetalleComprobanteDePago>();
					nuevoDetalle.add(detalle);
				}
			}
			venta.setUsuario(null);
			venta.getComprobanteDePago().setDetalleComprobanteDePagos(nuevoDetalle);
			listaVentasPeriodo.add(venta);
		}
		
		return  Obj.writeValueAsString(listaVentasPeriodo); 
	}
	
}
