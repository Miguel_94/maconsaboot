package com.Maconsa.Controllers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.ElementCollection;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.Maconsa.Entidad.Compra;
import com.Maconsa.Entidad.ComprobanteDePago;
import com.Maconsa.Entidad.DetalleComprobanteDePago;
import com.Maconsa.Entidad.Estado;
import com.Maconsa.Entidad.Producto;
import com.Maconsa.Entidad.TipoDeComprobante;
import com.Maconsa.Entidad.Usuario;
import com.Maconsa.Service.CompraServiceImpl;
import com.Maconsa.Service.ComprobanteDePagoServiceImpl;
import com.Maconsa.Service.DetalleComprobanteDePagoServiceImpl;
import com.Maconsa.Service.ProductoServiceImpl;
import com.Maconsa.Service.ProveedorServiceImpl;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class CompraController {
	
	ObjectMapper Obj = new ObjectMapper();
	
	@Autowired
	 private CompraServiceImpl compraServiceImpl;
	 @Autowired
	 private ProductoServiceImpl ProductoServiceImpl;
	 @Autowired
	 private DetalleComprobanteDePagoServiceImpl detalleComprobanteDePagoServiceImpl;	 
	 @Autowired
	 private ComprobanteDePago comprobanteDePago=new ComprobanteDePago();
	 @Autowired
	 private ProveedorServiceImpl proveedorServiceImpl;
	 @Autowired
	 private ComprobanteDePagoServiceImpl comprobanteDePagoServiceImpl; 
	 @ElementCollection
    private List<DetalleComprobanteDePago> detalleComprobante=  new ArrayList<DetalleComprobanteDePago>();

	 
	 @RequestMapping(value= {"/Compra/Listar"},method=RequestMethod.GET)
		public String listarCompras() throws JsonProcessingException, ParseException {
		
			return Obj.writeValueAsString(compraServiceImpl.listaDeCompras());
		}
	 
	 @RequestMapping(value= {"/Compra/LimpiarCarrito"},method=RequestMethod.GET)
		public String limpiarCarrito() throws JsonProcessingException, ParseException {
		
		 	
			return Obj.writeValueAsString("");
		}
	 
	@RequestMapping(value = "/Compra/AgregarDetalle" , method = RequestMethod.POST)
	public @ResponseBody String editUserpost(@RequestBody DetalleComprobanteDePago detalleComprobanteDePago) throws JsonParseException,IOException{
		
		DetalleComprobanteDePago detalleComprobanteDePagoOpe=new DetalleComprobanteDePago();
		Producto producto=ProductoServiceImpl.devolverProducto(detalleComprobanteDePago.getProducto().getIdProducto());
		int cantidad=detalleComprobanteDePago.getCantidadCompra();
		double valorCompra=detalleComprobanteDePago.getValorVenta();
		if(!verificarProductoRepetido(producto,cantidad,valorCompra))
		{
		
			detalleComprobanteDePagoOpe.setProducto(producto);
			detalleComprobanteDePagoOpe.setCantidadCompra(cantidad);
			detalleComprobanteDePagoOpe.setCantidadFinal(cantidad+producto.getCantidad());
			detalleComprobanteDePagoOpe.setSubtotal(cantidad*valorCompra);
			detalleComprobanteDePagoOpe.setValorVenta(valorCompra);
			detalleComprobante.add(detalleComprobanteDePago);
			 return Obj.writeValueAsString("Agregado Correctamente");
			
		}else {
			return Obj.writeValueAsString("Editado Correctamente");
		}
		
	}
	 
	@RequestMapping(value= {"/Compra/Guardar"},method=RequestMethod.POST)
	public String guardarCompra(@RequestBody Compra compra) throws JsonProcessingException, ParseException {
		
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
		Usuario usuario=new Usuario();
		Estado estado=new Estado();
		TipoDeComprobante tipoComprobante=new TipoDeComprobante();
		
		usuario.setIdUsuario(1);
		estado.setIdEstado(1);
		tipoComprobante.setIdTipoDeComprobanteDePago(compra.getComprobanteDePago().getTipodecomprobantedepago().getIdTipoDeComprobanteDePago());
		
		compra.getComprobanteDePago().setEstado(estado);
		compra.setProveedor(compra.getProveedor());
		compra.setEstado(estado);
		compra.setUsuario(usuario);
		
		compra.getComprobanteDePago().setTipodecomprobantedepago(tipoComprobante);
		comprobanteDePagoServiceImpl.ingresarComprobanteDePago(compra.getComprobanteDePago());
		
		for (DetalleComprobanteDePago detalleComprobanteDePago : detalleComprobante) {
			Producto producto=detalleComprobanteDePago.getProducto();
			int stockProducto=producto.getCantidad();
			int cantidadCompra=detalleComprobanteDePago.getCantidadCompra();
			int cantidadFinal=stockProducto+cantidadCompra;
			producto.setCantidad(cantidadFinal);
			ProductoServiceImpl.insertarProducto(producto);
			detalleComprobanteDePago.setComprobanteDePago(compra.getComprobanteDePago());
			detalleComprobanteDePago.setCantidadFinal(cantidadFinal);
			detalleComprobanteDePagoServiceImpl.ingrearDetalleComprobanteDePago(detalleComprobanteDePago);
		}
		
		compraServiceImpl.ingresarCompra(compra);
		detalleComprobante.clear();
		return Obj.writeValueAsString("Guardado Correctamente");
	}
	
	 private boolean verificarProductoRepetido(Producto producto,int cantidad,double valorCompra)
		{
			boolean existe=false;
			List<DetalleComprobanteDePago> detalleComprobantesOpe=  new ArrayList<DetalleComprobanteDePago>();
			for (DetalleComprobanteDePago detalleComprobanteDePago : detalleComprobante) {
				
				if(detalleComprobanteDePago.getProducto().getIdProducto()==producto.getIdProducto())
				{
					
					detalleComprobanteDePago.setProducto(producto);
					existe=true;
					detalleComprobanteDePago.setCantidadCompra(cantidad);
					detalleComprobanteDePago.setCantidadFinal(cantidad+producto.getCantidad());
					detalleComprobanteDePago.setSubtotal(cantidad*valorCompra);
					detalleComprobanteDePago.setValorVenta(valorCompra);
					detalleComprobantesOpe.add(detalleComprobanteDePago);
					
				}else {
					detalleComprobantesOpe.add(detalleComprobanteDePago);
				}
			}
			this.detalleComprobante=detalleComprobantesOpe;
			return existe;
		}
	 
		@RequestMapping(value= {"/Compra/listarCarrito"},method=RequestMethod.GET)
		public String listarCarrito() throws JsonProcessingException, ParseException {

		return Obj.writeValueAsString(detalleComprobante);
		}
			
		@RequestMapping(value= {"/Compra/Detalle"},method=RequestMethod.GET)
		@ResponseBody 
		public String DetalleCompra(HttpServletRequest request) throws JsonProcessingException, ParseException {
		
			int idCompra=Integer.parseInt(request.getParameter("idCompra"));
			return Obj.writeValueAsString(compraServiceImpl.verDetalleCompra(idCompra));
		}
		
		@RequestMapping(value = "/Compra/EliminarDetalle" , method = RequestMethod.POST)
		public String eliminarDetalle(@RequestBody DetalleComprobanteDePago detalleComprobanteDePago) throws JsonParseException,IOException{
			
			List<DetalleComprobanteDePago>listaEliminados=new ArrayList<DetalleComprobanteDePago>();
			for(DetalleComprobanteDePago detalle:detalleComprobante)
			{
				if (detalle.getProducto().getIdProducto()==detalleComprobanteDePago.getProducto().getIdProducto()){
	            	listaEliminados.add(detalle);
	            }
			}
			detalleComprobante.removeAll(listaEliminados);
			
			return Obj.writeValueAsString("Removido Correctamente");
		}
}
