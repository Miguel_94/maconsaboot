package com.Maconsa.Controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.Maconsa.Entidad.Producto;
import com.Maconsa.Entidad.Proveedor;
import com.Maconsa.Service.ProveedorServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProveedorController {

	ObjectMapper Obj = new ObjectMapper();
	
	 @Autowired
	 private ProveedorServiceImpl proveedorServiceImpl;
		
	 @RequestMapping(value= {"Proveedor/Listar"},method=RequestMethod.GET)
		@ResponseBody 
		public String listarProveedores() throws JsonProcessingException {
			return  Obj.writeValueAsString(proveedorServiceImpl.listaProveedores()); 
		}
	 
	 @RequestMapping(value= {"/Proveedor/Guardar"},method=RequestMethod.POST)
	    @ResponseBody
	    public String crearProveedor(@RequestBody Proveedor proveedor) throws IOException {
		 proveedorServiceImpl.insertarProveedor(proveedor);
	    	return  Obj.writeValueAsString("Proveedor Ingresado");
	    }
		
		@RequestMapping(value= {"/Proveedor/Eliminar{ruc}"},method=RequestMethod.GET)
	    @ResponseBody
	    public String eliminarProveedor(@PathVariable(value="ruc") String ruc) throws IOException {
			
			Proveedor proveedor= new Proveedor();
			proveedor.setRuc(ruc);
			proveedorServiceImpl.eliminarProveedor(proveedor);
	    	return  Obj.writeValueAsString("Proveedor eliminado");
	    }
		
		@RequestMapping(value= "/Proveedor/Buscar{descripcion}",method=RequestMethod.GET)
		@ResponseBody
		public String buscarProveedor(@PathVariable(value="descripcion") String descripcion, HttpServletRequest request ) throws JsonProcessingException
		{  
				ModelAndView modelAndView = new ModelAndView();

				int opcion=Integer.parseInt(request.getParameter("opcion"));
				if(opcion==1 && !descripcion.equals(""))
				{
					List<Proveedor> listaProveedor=proveedorServiceImpl.buscarProveedorPorRuc("%"+descripcion+"%");
					return  Obj.writeValueAsString(listaProveedor);
				}
				else if(opcion==2 && !descripcion.equals("")) {
					
					List<Proveedor> listaProveedor=proveedorServiceImpl.buscarProveedorPorDenominacion("%"+descripcion+"%");
			        return  Obj.writeValueAsString(listaProveedor);
				}else {
					return  Obj.writeValueAsString("");
				}
				
		}
}
