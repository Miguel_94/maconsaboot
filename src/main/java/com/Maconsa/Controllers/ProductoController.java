package com.Maconsa.Controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.Maconsa.Entidad.Paginacion;
import com.Maconsa.Entidad.Producto;
import com.Maconsa.Entidad.Usuario;
import com.Maconsa.Service.IProductoService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ProductoController {
	
	ObjectMapper Obj = new ObjectMapper();
	
	 @Autowired
	 private IProductoService productoService;

	@RequestMapping(value= {"/Producto/Listar"},method=RequestMethod.GET)
    @ResponseBody
    public String listarProducto() throws JsonProcessingException {
    	return  Obj.writeValueAsString(productoService.findAll()); 
    }
	
	/*
	@RequestMapping(value= {"/Producto/ListarPaginado"},method=RequestMethod.GET)
    @ResponseBody
    public String listarProductoPaginado(HttpServletRequest request) throws JsonProcessingException {
		
		int posicion=Integer.parseInt(request.getParameter("posicion"));
		int tamanioPagina=Integer.parseInt(request.getParameter("tamanioPagina"));
		int numeroPagina=Integer.parseInt(request.getParameter("numeroPagina"));
		int totalRegistros=productoService.devolverTotalDeProductos();
		int totalPaginas=totalRegistros/tamanioPagina;
		Paginacion paginacion=new Paginacion();
		paginacion.setTamanioPagina(tamanioPagina);
		paginacion.setPrimeraFila(posicion);
		paginacion.setUltimaFila(numeroPagina*tamanioPagina);
		paginacion.setNumeroPagina(numeroPagina);
		if(totalRegistros%tamanioPagina!=0)
		{
			totalPaginas=totalPaginas+1;
		}
		paginacion.setTotalPaginas(totalPaginas);
		paginacion.setListaFilas(productoService.listaDeProductos(posicion,tamanioPagina));
    	return  Obj.writeValueAsString(paginacion); 
    }*/
	
	@RequestMapping(value= {"/Producto/ListarPaginado"},method=RequestMethod.POST)
    @ResponseBody
    public String listarProductoPaginado(@RequestBody Paginacion paginacion) throws JsonProcessingException {
		
		int totalRegistros=productoService.devolverTotalDeProductos();
		int totalPaginas=totalRegistros/paginacion.getTamanioPagina();
		int ultimaFila=paginacion.getNumeroPagina()*paginacion.getTamanioPagina();
		paginacion.setUltimaFila(ultimaFila);
		if(totalRegistros%paginacion.getTamanioPagina()!=0)
		{
			totalPaginas=totalPaginas+1;
		}
		paginacion.setTotalPaginas(totalPaginas);
		paginacion.setTotalRegistros(totalRegistros);
		paginacion.setListaFilas(productoService.listaDeProductos(paginacion.getPrimeraFila(),paginacion.getTamanioPagina()));
    	return  Obj.writeValueAsString(paginacion); 
    }
	
	@RequestMapping(value= {"/Producto/Guardar"},method=RequestMethod.POST)
    @ResponseBody
    public String crearProducto(@RequestBody Producto producto) throws IOException {
		productoService.insertarProducto(producto);
    	return  Obj.writeValueAsString(producto);
    }
	
	@RequestMapping(value= {"/Producto/Eliminar{idProducto}"},method=RequestMethod.GET)
    @ResponseBody
    public String eliminarProducto(@PathVariable(value="idProducto") int idProducto) throws IOException {
		
		//int idProducto=Integer.parseInt(request.getParameter("idProducto"));
		Producto producto = new Producto();
		producto.setIdProducto(idProducto);
		productoService.eliminarProducto(producto);
    	return  Obj.writeValueAsString("Producto eliminado");
    }
	
	@RequestMapping(value= "/Producto/Buscar{descripcion}",method=RequestMethod.GET)
	@ResponseBody
	public String buscarProducto(@PathVariable(value="descripcion") String descripcion, HttpServletRequest request ) throws JsonProcessingException
	{  
			ModelAndView modelAndView = new ModelAndView();

			int opcion=Integer.parseInt(request.getParameter("opcion"));
			if(opcion==1 && !descripcion.equals(""))
			{
				List<Producto> listaProductos=productoService.buscarProductoPorID("%"+descripcion+"%");
				return  Obj.writeValueAsString(listaProductos);
			}
			else if(opcion==2 && !descripcion.equals("")) {
				
				List<Producto> listaProductos=productoService.buscarProductoPorDescripcion("%"+descripcion+"%");
		        return  Obj.writeValueAsString(listaProductos);
			}else {
				return  Obj.writeValueAsString("");
			}
			
	}

}
