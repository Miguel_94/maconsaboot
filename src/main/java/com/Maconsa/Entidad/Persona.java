package com.Maconsa.Entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="persona") 
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Persona {

	@Id
	@Column(name="dni", nullable = false)
	private String dni;
	
	@Column(name="nombres", nullable = false, length = 45)
	private String nombres;
	
	@Column(name="apellidos", nullable = false, length = 45)
	private String apellidos;
	
	@Column(name="telefono", nullable = false, length = 9)
	private String telefono;
	
	@Column(name="imagen", nullable = false, length = 80)
	private String imagen;
	

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	
	
	
	
}
