package com.Maconsa.Entidad;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="proveedor")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Proveedor {
	
	@Id
	@Column(name="ruc", nullable = false, length = 11)
	private String ruc;
	
	@Column(name="denominacion", nullable = false, length = 80)
    private String denominacion;
	
	@Column(name="direccion", nullable = false, length = 100)
    private String direccion;
	
	@Column(name="telefono", nullable = false, length = 9)
    private String telefono;
	
	@OneToOne
    @JoinColumn(name ="idEstado", nullable = false)
    private Estado estado;
	
    
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
		
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
    

}
