package com.Maconsa.Entidad;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Kardex")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Kardex {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idKardex", nullable = false)
	private int idKardex;
	
	@OneToOne
	@JoinColumn(name ="idProducto", nullable = false)
	private Producto producto;
	
	@Transient
    private List<Compra>listaCompras;
    
	@Transient
    private List<Venta>listaVentas;
    
	@Column(name="stockInicial", nullable = false)
    private int stockInicial;
	
	@Column(name="fechaInicio", nullable = false)
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fechaInicio;
   
	public int getIdInventario() {
		return idKardex;
	}
	public void setIdInventario(int idKardex) {
		this.idKardex = idKardex;
	}
	public List<Compra> getListaCompras() {
		return listaCompras;
	}
	public void setListaCompras(List<Compra> listaCompras) {
		this.listaCompras = listaCompras;
	}
	public List<Venta> getListaVentas() {
		return listaVentas;
	}
	public void setListaVentas(List<Venta> listaVentas) {
		this.listaVentas = listaVentas;
	}
	public int getIdKardex() {
		return idKardex;
	}
	public void setIdKardex(int idKardex) {
		this.idKardex = idKardex;
	}
	public int getStockInicial() {
		return stockInicial;
	}
	public void setStockInicial(int stockInicial) {
		this.stockInicial = stockInicial;
	}
	
	
	
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public double totalComprasSinIgv()
	{
		double totalSinIgvCompra=0;
		for(Compra c : listaCompras)
		{
			for(DetalleComprobanteDePago dcp : c.getComprobanteDePago().getDetalleComprobanteDePagos())
			{
				totalSinIgvCompra+=dcp.getSubtotal();
			}
		}
		return totalSinIgvCompra;
	}
	
	
	public double totalVentasSinIgv()
	{
		double totalSinIgvVenta=0;
		for(Venta v : listaVentas)
		{
			for(DetalleComprobanteDePago dcp : v.getComprobanteDePago().getDetalleComprobanteDePagos())
			{
				totalSinIgvVenta+=dcp.getSubtotal();
			}
		}
		return totalSinIgvVenta;
	}
	
	public int totalProductosCompra()
	{
		int totalProductosCompra=0;
		for(Compra c : listaCompras)
		{
			for(DetalleComprobanteDePago dcp : c.getComprobanteDePago().getDetalleComprobanteDePagos())
			{
				totalProductosCompra+=dcp.getCantidadCompra();
			}
		}
		return totalProductosCompra;
	}
	
	
	public int totalProductosVenta()
	{
		int totalProductosVenta=0;
		for(Venta v : listaVentas)
		{
			for(DetalleComprobanteDePago dcp : v.getComprobanteDePago().getDetalleComprobanteDePagos())
			{
				totalProductosVenta+=dcp.getCantidadCompra();
			}
		}
		return totalProductosVenta;
	}
    
    
}
