package com.Maconsa.Entidad;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Comprobantedepago") 
@Component
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class ComprobanteDePago {

	@Id
	@Column(name="idcomprobantedepago")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idComprobanteDePago;
	
	@JoinColumn(name="idtipodecomprobantedepago")
	@OneToOne
    private TipoDeComprobante tipodecomprobantedepago;
	
	@Column(name = "numero")
    private String  numero;
    
	@Column(name = "serie")
    private String serie;
	
	@OneToOne
    @JoinColumn(name ="idEstado", nullable = false)
    private Estado estado;
    
	//@Transient
	@OneToMany(mappedBy = "comprobanteDePago", cascade = CascadeType.MERGE, orphanRemoval = true)
    private List<DetalleComprobanteDePago> detalleComprobanteDePagos;
    
	public int getIdComprobanteDePago() {
		return idComprobanteDePago;
	}
	public void setIdComprobanteDePago(int idComprobanteDePago) {
		this.idComprobanteDePago = idComprobanteDePago;
	}
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	public List<DetalleComprobanteDePago> getDetalleComprobanteDePagos() {
		return detalleComprobanteDePagos;
	}
	public void setDetalleComprobanteDePagos(List<DetalleComprobanteDePago> detalleComprobanteDePagos) {
		this.detalleComprobanteDePagos = detalleComprobanteDePagos;
	}
	public TipoDeComprobante getTipodecomprobantedepago() {
		return tipodecomprobantedepago;
	}
	public void setTipodecomprobantedepago(TipoDeComprobante tipodecomprobantedepago) {
		this.tipodecomprobantedepago = tipodecomprobantedepago;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	
   
}
