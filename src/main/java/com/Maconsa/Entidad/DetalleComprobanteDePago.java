package com.Maconsa.Entidad;

import javax.persistence.*;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@Embeddable
@Entity
@Table(name="detallecomprobantedepago")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class DetalleComprobanteDePago {
	
	//@EmbeddedId
	@Id
	@Column(name="iddetallecomprobante")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idDetalleComprobante;
	
	@JsonIgnore
	@JoinColumn(name="idcomprobantedepago")
	@ManyToOne
	private ComprobanteDePago comprobanteDePago;
	
	@JoinColumn(name = "idproducto")
	@OneToOne
    private Producto producto;
	
	@Column(name="subtotal")
    private double subtotal;
	
	@Column(name="cantidadcompra")
    private int cantidadCompra; 
	
	@Column(name="valorventa")
    private double valorVenta;
	
	@Column(name="cantidadfinal")
    private double cantidadFinal;
    

	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	public int getCantidadCompra() {
		return cantidadCompra;
	}
	public void setCantidadCompra(int cantidadCompra) {
		this.cantidadCompra = cantidadCompra;
	}
	public double getValorVenta() {
		return valorVenta;
	}
	public void setValorVenta(double valorVenta) {
		this.valorVenta = valorVenta;
	}
	public double getCantidadFinal() {
		return cantidadFinal;
	}
	public void setCantidadFinal(double cantidadFinal) {
		this.cantidadFinal = cantidadFinal;
	}
	public int getIdDetalleComprobante() {
		return idDetalleComprobante;
	}
	public void setIdDetalleComprobante(int idDetalleComprobante) {
		this.idDetalleComprobante = idDetalleComprobante;
	}
	public ComprobanteDePago getComprobanteDePago() {
		return comprobanteDePago;
	}
	public void setComprobanteDePago(ComprobanteDePago comprobanteDePago) {
		this.comprobanteDePago = comprobanteDePago;
	}
	
	
	
}
