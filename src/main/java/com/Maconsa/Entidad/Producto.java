package com.Maconsa.Entidad;

import javax.persistence.*;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="producto") 
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Producto {
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 @Column(name="idProducto", nullable = false)
	 private int idProducto;
	 
	 public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	 @Column(name="descripcion", nullable = false, length = 80)
     private String descripcion;
	 

	 @Column(name="unidadDeMedida", nullable = false, length = 50)
     private String unidadDeMedida;
	 

	 @Column(name="cantidad", nullable = false)
     private int cantidad;

	 @Column(name="valorVenta", nullable = false)
     private double valorVenta;
	 
	 @OneToOne
     @JoinColumn(name ="idEstado", nullable = false)
     private Estado estado;
     
     
	public int getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getUnidadDeMedida() {
		return unidadDeMedida;
	}
	public void setUnidadDeMedida(String unidadDeMedida) {
		this.unidadDeMedida = unidadDeMedida;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getValorVenta() {
		return valorVenta;
	}
	public void setValorVenta(double valorVenta) {
		this.valorVenta = valorVenta;
	}
   
	    
}
