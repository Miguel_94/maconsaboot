package com.Maconsa.Entidad;



import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name="compra") 
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Compra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idCompra", nullable = false)
	private int idCompra;
	
	
	@JoinColumn(name="idcomprobantedepago")
	@OneToOne
    private ComprobanteDePago comprobanteDePago;
	
	
	@JoinColumn(name="ruc")
	@OneToOne
    private Proveedor proveedor;
    
	@JoinColumn(name="idUsuario")
	@OneToOne
    private Usuario usuario;
    
	@Column(name="fecha",nullable = false, updatable = false)
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	//@DateTimeFormat(pattern="yyyy-MM-dd")
    
	
	 @OneToOne
     @JoinColumn(name ="idEstado", nullable = false)
     private Estado estado;
    
	public int getIdCompra() {
		return idCompra;
	}
	public void setIdCompra(int idCompra) {
		this.idCompra = idCompra;
	}
	public ComprobanteDePago getComprobanteDePago() {
		return comprobanteDePago;
	}
	public void setComprobanteDePago(ComprobanteDePago comprobanteDePago) {
		this.comprobanteDePago = comprobanteDePago;
	}
	public Proveedor getProveedor() {
		return proveedor;
	}
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
    
   
}
