package com.Maconsa.Entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import net.bytebuddy.build.ToStringPlugin.Exclude;

@Entity
@Table(name="usuario") 
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Usuario {
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 @Column(name="idusuario", nullable = false)
	 private int idUsuario;
	 
	 @OneToOne
     @JoinColumn(name ="dni", nullable = false)
	 private Persona persona;
	 
	 @Column(name="nombreusuario", nullable = false, length = 50)
	 private String nombreUsuario;
	 
	 @Column(name="password", nullable = false, length = 50)
	 private String password;  
	 
	 @Transient
	 private String confirmarpassword;
	 
	 @Column(name="rutadealmacenamientoreportes", nullable = false, length = 100)
	 private String rutaDeAlmacenamientoReportes;
	 
	 @OneToOne
     @JoinColumn(name ="idEstado", nullable = false)
     private Estado estado;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmarpassword() {
		return confirmarpassword;
	}

	public void setConfirmarpassword(String confirmarpassword) {
		this.confirmarpassword = confirmarpassword;
	}

	public String getRutaDeAlmacenamientoReportes() {
		return rutaDeAlmacenamientoReportes;
	}

	public void setRutaDeAlmacenamientoReportes(String rutaDeAlmacenamientoReportes) {
		this.rutaDeAlmacenamientoReportes = rutaDeAlmacenamientoReportes;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	 

	
	
	
	 
	 
	 

}
