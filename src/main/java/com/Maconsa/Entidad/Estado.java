package com.Maconsa.Entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name="estado") 
public class Estado {
	
	 @Id
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 @NotEmpty
	 @Column(name="idEstado")
	 private int idEstado;
	
	 
	 @NotEmpty
	 @Column(name="descripcion",nullable = false, length = 50)
     private String descripcion;


	public int getIdEstado() {
		return idEstado;
	}


	public void setIdEstado(int idEstado) {
		this.idEstado = idEstado;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	 
	 
	 
	

}
