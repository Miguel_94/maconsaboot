package com.Maconsa.Entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="tipodeusuario")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class TipoDeUsuario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idtipodeusuario")
	private int idTipoDeUsuario;
	 
	@Column(name="destipodeusuario",nullable = false, length = 50)
	private String desTipoDeUsuario;
	
	
	public int getIdTipoDeUsuario() {
		return idTipoDeUsuario;
	}
	public void setIdTipoDeUsuario(int idTipoDeUsuario) {
		this.idTipoDeUsuario = idTipoDeUsuario;
	}
	public String getDesTipoDeUsuario() {
		return desTipoDeUsuario;
	}
	public void setDesTipoDeUsuario(String desTipoDeUsuario) {
		this.desTipoDeUsuario = desTipoDeUsuario;
	}

}
