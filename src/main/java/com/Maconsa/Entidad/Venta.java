package com.Maconsa.Entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="venta") 
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Venta {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idVenta;
	
	@JoinColumn(name="idcomprobantedepago")
	@OneToOne
	private ComprobanteDePago comprobanteDePago;
	
	@Column(name="cliente",nullable = false, length = 50)
	private String cliente;
	
	@Column(name="ruc", length = 50)
	private String rucCliente;
	
	@JoinColumn(name="idUsuario")
	@OneToOne
	private Usuario usuario;
	
	@Column(name="fecha")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private String fecha;
	
	 @OneToOne
     @JoinColumn(name ="idEstado", nullable = false)
     private Estado estado;
	  
	  
	public int getIdVenta() {
		return idVenta;
	}
	public void setIdVenta(int idVenta) {
		this.idVenta = idVenta;
	}
	public ComprobanteDePago getComprobanteDePago() {
		return comprobanteDePago;
	}
	public void setComprobanteDePago(ComprobanteDePago comprobanteDePago) {
		this.comprobanteDePago = comprobanteDePago;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Estado getEstado() {
		return estado;
	}
	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	   
	

}
