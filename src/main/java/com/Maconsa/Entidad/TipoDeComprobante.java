package com.Maconsa.Entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="Tipodecomprobante")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class TipoDeComprobante {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="idtipodecomprobantedepago", nullable = false)
	private int idTipoDeComprobanteDePago;
	
	@Column(name="descTipo", nullable = false, length = 40)
    private String descTipoDeComprobanteDePago;
     
    
	public int getIdTipoDeComprobanteDePago() {
		return idTipoDeComprobanteDePago;
	}
	public void setIdTipoDeComprobanteDePago(int idTipoDeComprobanteDePago) {
		this.idTipoDeComprobanteDePago = idTipoDeComprobanteDePago;
	}
	public String getDescTipoDeComprobanteDePago() {
		return descTipoDeComprobanteDePago;
	}
	public void setDescTipoDeComprobanteDePago(String descTipoDeComprobanteDePago) {
		this.descTipoDeComprobanteDePago = descTipoDeComprobanteDePago;
	}
    
    
	
}
