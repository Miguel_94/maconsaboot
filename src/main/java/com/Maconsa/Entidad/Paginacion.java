package com.Maconsa.Entidad;

import java.util.ArrayList;
import java.util.List;


public class Paginacion {
	
	private int primeraFila;
	private int totalPaginas;
	private int tamanioPagina;
	private int ultimaFila;
	private int numeroPagina;
	private int totalRegistros;
	private List listaFilas=new ArrayList<>();

	


	public int getTotalRegistros() {
		return totalRegistros;
	}
	public void setTotalRegistros(int totalRegistros) {
		this.totalRegistros = totalRegistros;
	}
	public List getListaFilas() {
		return listaFilas;
	}
	public void setListaFilas(List listaFilas) {
		this.listaFilas = listaFilas;
	}
	public int getTotalPaginas() {
		return totalPaginas;
	}
	public void setTotalPaginas(int totalPaginas) {
		this.totalPaginas = totalPaginas;
	}
	public int getTamanioPagina() {
		return tamanioPagina;
	}
	public void setTamanioPagina(int tamanioPagina) {
		this.tamanioPagina = tamanioPagina;
	}
	public int getNumeroPagina() {
		return numeroPagina;
	}
	public void setNumeroPagina(int numeroPagina) {
		this.numeroPagina = numeroPagina;
	}

	public int getPrimeraFila() {
		return primeraFila;
	}
	public void setPrimeraFila(int primeraFila) {
		this.primeraFila = primeraFila;
	}
	public int getUltimaFila() {
		return ultimaFila;
	}
	public void setUltimaFila(int ultimaFila) {
		this.ultimaFila = ultimaFila;
	}
	
	

}
