package com.Maconsa.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Producto;

public interface IProductoService {
	
	public void insertarProducto(Producto producto);
	 public int devolverTotalDeProductos();
	 public List<Producto> listaDeProductos(int indice,int tamanioPagina);
	 public double devolverStockActual(int idProducto);
	 public Producto devolverProducto(int idProducto);
	 public List<Producto> buscarProductoPorDescripcion(String descripcion);
	 public List<Producto> buscarProductoPorID(String descripcion);
	 public void editarProducto(Producto producto);
	 public void eliminarProducto(Producto producto);
	 public int obtenerUltimoIDProducto();
	 public List<Producto> findAll();

}
