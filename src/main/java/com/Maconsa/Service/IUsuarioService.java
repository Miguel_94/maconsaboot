package com.Maconsa.Service;

import java.util.List;

import com.Maconsa.Entidad.Usuario;

public interface IUsuarioService {
	 public void insertarUsuario(Usuario usuario);
	 public int devolverTotalDeUsuarios();
	 public List<Usuario> listarUsuarios();
	 public Usuario devolverUsuario(int idUsuario);
	 public List<Usuario> buscarUsuarioPorDescripcion(String descripcion);
	 public List<Usuario> buscarUsuarioPorID(String descripcion);
	 public void editarUsuario(Usuario usuario);
	 public void eliminarUsuario(Usuario usuario);
	 public int obtenerUltimoIDUsuario();

}
