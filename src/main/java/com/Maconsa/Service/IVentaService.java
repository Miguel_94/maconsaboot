package com.Maconsa.Service;

import java.util.List;

import com.Maconsa.Entidad.Venta;

public interface IVentaService {
	public List<Venta> listaDeVentas(int posicion);
	public void EliminarVenta(int idCompra);
	public Venta verDetalleVenta(int idCompra);
	public void ingresarVenta(Venta compra);
	public int devolverTotalDeVentas();
}
