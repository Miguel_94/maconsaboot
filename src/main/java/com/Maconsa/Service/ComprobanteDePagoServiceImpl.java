package com.Maconsa.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.ComprobanteDePago;
import com.Maconsa.Persistencia.ComprobanteDePagoDao;

@Service
public class ComprobanteDePagoServiceImpl implements IComprobanteDePagoService {

	@Autowired
	private ComprobanteDePagoDao comprobanteDePagoDao;

	@Override
	public void ingresarComprobanteDePago(ComprobanteDePago comprobanteDePago) {
		
		comprobanteDePagoDao.save(comprobanteDePago);
		
	}

	@Override
	public double devolverUltimoIDComprobante() {
		// TODO Auto-generated method stub
		return comprobanteDePagoDao.devolverUltimoIDComprobante();
	}
	
	
}
