package com.Maconsa.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.DetalleComprobanteDePago;
import com.Maconsa.Persistencia.DetalleComprobanteDePagoDao;

@Service
public class DetalleComprobanteDePagoServiceImpl implements IDetalleComprobanteDePagoService{

	@Autowired
	private DetalleComprobanteDePagoDao detalleComprobanteDePagoDao;

	@Override
	public void ingrearDetalleComprobanteDePago(DetalleComprobanteDePago detalleComprobanteDePago) {

		detalleComprobanteDePagoDao.save(detalleComprobanteDePago);
		
	}
}
