package com.Maconsa.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Compra;
import com.Maconsa.Entidad.Kardex;
import com.Maconsa.Entidad.Producto;
import com.Maconsa.Persistencia.InventarioDao;

@Service
public class InventarioServiceImpl implements IInventarioService{
	
	@Autowired
	private InventarioDao inventarioDao;

	@Override
	public double devolverCostoInventario() {
		// TODO Auto-generated method stub
		return inventarioDao.devolverCostoInventario();
	}


	@Override
	public Kardex ObtenerStockInicial(int mes, int año, Producto producto) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ArrayList<Integer> listaIdComprasPeriodo(int año, int mes, int idProducto) {
		// TODO Auto-generated method stub
		return inventarioDao.devolveridCompras(año,mes,idProducto);
	}


	@Override
	public ArrayList<Integer> listaIdVentasPeriodo() {
		// TODO Auto-generated method stub
		return inventarioDao.devolveridVentas();
	}


	@Override
	public void agregarKardexProducto(Producto producto) {
		Kardex kardex=new Kardex();
		Date fechaActual=new Date();
		
		kardex.setStockInicial(producto.getCantidad());
		kardex.setProducto(producto);
		kardex.setFechaInicio(fechaActual);
		
		inventarioDao.save(kardex);
		
	}


	@Override
	public String devolverStockInicialProducto(int año, int mes, int idProducto) {
		return inventarioDao.devolverStockInicialProducto(año,mes,idProducto);
	}

	

}
