package com.Maconsa.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Estado;
import com.Maconsa.Entidad.Usuario;
import com.Maconsa.Persistencia.PersonaDao;
import com.Maconsa.Persistencia.UsuarioDao;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private UsuarioDao usuarioDao;
	@Autowired
	private PersonaDao personaDao;
	
	@Override
	public void insertarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		personaDao.save(usuario.getPersona());
		usuarioDao.save(usuario);
	}

	@Override
	public int devolverTotalDeUsuarios() {
		// TODO Auto-generated method stub
		return usuarioDao.devolverTotalDeUsuarios();
	}

	@Override
	public List<Usuario> listarUsuarios() {
		// TODO Auto-generated method stub
		return usuarioDao.findAll();
	}

	@Override
	public Usuario devolverUsuario(int idUsuario) {
		// TODO Auto-generated method stub
		return usuarioDao.getOne(idUsuario);
	}

	@Override
	public List<Usuario> buscarUsuarioPorDescripcion(String descripcion) {
		// TODO Auto-generated method stub
		return usuarioDao.buscarUsuarioPorDescripcion(descripcion);
	}

	@Override
	public List<Usuario> buscarUsuarioPorID(String descripcion) {
		// TODO Auto-generated method stub
		return usuarioDao.buscarUsuarioPorID(descripcion);
	}

	@Override
	public void editarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		personaDao.save(usuario.getPersona());
		usuarioDao.save(usuario);
	}

	@Override
	public void eliminarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		usuario=usuarioDao.getOne(usuario.getIdUsuario());
		Estado estado=new Estado();
		estado.setIdEstado(2);
		usuario.setEstado(estado);
		usuarioDao.save(usuario);
	}

	@Override
	public int obtenerUltimoIDUsuario() {
		// TODO Auto-generated method stub
		return usuarioDao.obtenerUltimoIDUsuario();
	}

}
