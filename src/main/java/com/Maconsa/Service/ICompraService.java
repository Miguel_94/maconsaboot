package com.Maconsa.Service;

import java.util.List;

import com.Maconsa.Entidad.Compra;


public interface ICompraService {

	public List<Compra> listaDeCompras();
	public List<Compra> buscarCompras(String descripcion);
	public void EliminarCompra(int idCompra);
	public Compra verDetalleCompra(int idCompra);
	public void ingresarCompra(Compra compra);
	public int devolverTotalDeCompras();
	
}
