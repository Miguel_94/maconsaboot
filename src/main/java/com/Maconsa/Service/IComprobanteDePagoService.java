package com.Maconsa.Service;

import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.ComprobanteDePago;
@Repository
public interface IComprobanteDePagoService {
	
	public void ingresarComprobanteDePago(ComprobanteDePago comprobanteDePago);
	public double devolverUltimoIDComprobante();

}
