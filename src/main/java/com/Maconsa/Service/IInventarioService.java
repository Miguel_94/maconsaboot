package com.Maconsa.Service;

import java.util.ArrayList;
import java.util.List;

import com.Maconsa.Entidad.Compra;
import com.Maconsa.Entidad.Kardex;
import com.Maconsa.Entidad.Producto;

public interface IInventarioService{
	
	public double devolverCostoInventario();	
	public Kardex ObtenerStockInicial(int mes, int año,Producto producto);
	public ArrayList<Integer> listaIdComprasPeriodo(int año, int mes, int idProducto);
	public ArrayList<Integer> listaIdVentasPeriodo();
	public void agregarKardexProducto(Producto producto);
	public String devolverStockInicialProducto(int año, int mes, int idProducto);

}
