package com.Maconsa.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Compra;
import com.Maconsa.Persistencia.CompraDao;

@Service
public class CompraServiceImpl implements ICompraService{

	@Autowired
	private CompraDao compraDao; 
	@Override
	public List<Compra> listaDeCompras() {
		// TODO Auto-generated method stub
		return compraDao.findAll();
	}
 
	@Override
	public void EliminarCompra(int idCompra) {
		// TODO Auto-generated method stub
	

	}

	@Override
	public Compra verDetalleCompra(int idCompra) {
		// TODO Auto-generated method stub
		return compraDao.getOne(idCompra);
	}

	@Override
	public void ingresarCompra(Compra compra) {
		// TODO Auto-generated method stub
		compraDao.save(compra);
	}

	@Override
	public int devolverTotalDeCompras() {
		// TODO Auto-generated method stub
		return compraDao.devolverTotalDeCompras();
	}

	@Override
	public List<Compra> buscarCompras(String descripcion) {
		// TODO Auto-generated method stub
		return compraDao.buscarCompras(descripcion);
	}

}
