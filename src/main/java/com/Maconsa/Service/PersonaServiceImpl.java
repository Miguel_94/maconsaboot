package com.Maconsa.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Persona;
import com.Maconsa.Persistencia.PersonaDao;

@Service
public class PersonaServiceImpl implements IPersonaService {
	
	@Autowired
	private PersonaDao personaDao;

	@Override
	public void ingresarPersona(Persona persona) {
		// TODO Auto-generated method stub
		personaDao.save(persona);

	}

	@Override
	public Persona buscarPersona(String dni) {
		// TODO Auto-generated method stub
		return personaDao.getOne(dni);
	}

}
