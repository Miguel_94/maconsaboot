package com.Maconsa.Service;

import java.util.List;

import com.Maconsa.Entidad.Proveedor;

public interface IProveedorService {
	
	public void insertarProveedor(Proveedor proveedor);
	 public int devolverTotalDeProveedor();
	 public List<Proveedor> listaProveedor(int indice);
	 public List<Proveedor> listaProveedores();
	 public Proveedor devolverProveedor(String ruc);
	 public List<Proveedor> buscarProveedorPorDenominacion(String descripcion);
	 public List<Proveedor> buscarProveedorPorRuc(String descripcion);
	 public void editarProveedor(Proveedor proveedor);
	 public void eliminarProveedor(Proveedor proveedor);
}
