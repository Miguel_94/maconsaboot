package com.Maconsa.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Estado;
import com.Maconsa.Entidad.Producto;
import com.Maconsa.Persistencia.ProductoDao;


@Service
public class ProductoServiceImpl implements IProductoService {

	@Autowired
	private ProductoDao productoDao;
	
	@Override
	public void insertarProducto(Producto producto) {
		// TODO Auto-generated method stub
		 productoDao.save(producto);
	}

	@Override
	public int devolverTotalDeProductos() {
		// TODO Auto-generated method stub
		return productoDao.devolverTotalDeProductos();
	}

	@Override
	public List<Producto> listaDeProductos(int indice,int tamanioPagina) {
		// TODO Auto-generated method stub
		return productoDao.listaDeProductos(indice,tamanioPagina);
	}

	@Override
	public double devolverStockActual(int idProducto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Producto devolverProducto(int idProducto) {
		// TODO Auto-generated method stub
		return productoDao.getOne(idProducto);
	}

	@Override
	public List<Producto> buscarProductoPorDescripcion(String descripcion) {
		// TODO Auto-generated method stub
		return productoDao.buscarProductoPorDescripcion(descripcion);
	}
	
	@Override
	public List<Producto> buscarProductoPorID(String descripcion){
		// TODO Auto-generated method stub
		
		return productoDao.buscarProductoPorID(descripcion);
	}

	@Override
	public void editarProducto(Producto producto) {
		
		productoDao.save(producto);
	}

	@Override
	public void eliminarProducto(Producto producto) {
		// TODO Auto-generated method stub
		producto= productoDao.getOne(producto.getIdProducto());
		Estado estado=new Estado();
		estado.setIdEstado(2);
		producto.setEstado(estado); 
		productoDao.save(producto);
	}

	@Override
	public int obtenerUltimoIDProducto() {
		
		return productoDao.obtenerUltimoIDProducto();

	}

	@Override
	public List<Producto> findAll() {
		// TODO Auto-generated method stub
		return productoDao.findAll();
	}
	
	

}
