package com.Maconsa.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Estado;
import com.Maconsa.Entidad.Proveedor;
import com.Maconsa.Persistencia.ProveedorDao;

@Service
public class ProveedorServiceImpl implements IProveedorService{
	
	@Autowired
	private ProveedorDao proveedorDao;
	
	@Override
	public void insertarProveedor(Proveedor proveedor) {
		// TODO Auto-generated method stub
		Estado estado=new Estado();
		estado.setIdEstado(1);
		proveedor.setEstado(estado);
		proveedorDao.save(proveedor);
	}

	@Override
	public int devolverTotalDeProveedor() {
		// TODO Auto-generated method stub
		return proveedorDao.devolverTotalDeProveedor();
	}

	@Override
	public List<Proveedor> listaProveedor(int indice) {
		// TODO Auto-generated method stub
		return proveedorDao.listaProveedor(indice);
	}

	@Override
	public List<Proveedor> listaProveedores() {
		// TODO Auto-generated method stub
		return proveedorDao.findAll();
	}

	@Override
	public Proveedor devolverProveedor(String ruc) {
		// TODO Auto-generated method stub
		return proveedorDao.getOne(ruc);
	}


	@Override
	public void editarProveedor(Proveedor proveedor) {
		Estado estado=new Estado();
		estado.setIdEstado(1);
		proveedor.setEstado(estado);
		proveedorDao.save(proveedor);
	}

	@Override
	public void eliminarProveedor(Proveedor proveedor) {
		// TODO Auto-generated method stub
		Estado estado=new Estado();
		proveedor=proveedorDao.getOne(proveedor.getRuc());
		estado.setIdEstado(2);
		proveedor.setEstado(estado);
		proveedorDao.save(proveedor);
	}

	@Override
	public List<Proveedor> buscarProveedorPorDenominacion(String descripcion) {
		// TODO Auto-generated method stub
		return proveedorDao.buscarProveedorPorDescripcion(descripcion);
	}

	@Override
	public List<Proveedor> buscarProveedorPorRuc(String descripcion) {
		// TODO Auto-generated method stub
		return proveedorDao.buscarProveedorPorRuc(descripcion);
	}
	
	 

}
