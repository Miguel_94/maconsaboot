package com.Maconsa.Service;

import com.Maconsa.Entidad.Persona;

public interface IPersonaService {

	public void ingresarPersona(Persona persona);
	
	public Persona buscarPersona(String dni);
	
}
