package com.Maconsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaconsaAngularBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaconsaAngularBootApplication.class, args);
	}

}
