package com.Maconsa.Persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.Compra;
@Repository
public interface CompraDao extends JpaRepository<Compra, Integer>{
	
	@Query(nativeQuery = true, value="select * from compra group BY id_compra ASC  HAVING id_Estado=1 limit ?1,10")
	public List<Compra> listaDeCompras(int posicion);
	
	
	//public Compra verDetalleCompra(int idCompra);
	
	@Query(nativeQuery = true, value="select count(*) from compra where id_Estado=1")
	public int devolverTotalDeCompras();
	

	@Query(nativeQuery = true, value="SELECT * FROM compra c inner join proveedor p on (c.ruc=p.ruc) WHERE p.denominacion LIKE ?1 and c.id_estado=1")
	public List<Compra> buscarCompras(String descripcion);

}
