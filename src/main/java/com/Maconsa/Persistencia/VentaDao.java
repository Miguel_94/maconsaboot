package com.Maconsa.Persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.Venta;
@Repository
public interface VentaDao extends JpaRepository<Venta, Integer>{
	
	@Query(nativeQuery = true, value="select * from venta group BY id_venta ASC  HAVING id_estado=1 limit ?1,10")
	public List<Venta> listaDeVentas(int posicion);
	
	@Query(nativeQuery = true, value="select count(*) from venta where id_estado=1")
	public int devolverTotalDeVentas();

}
