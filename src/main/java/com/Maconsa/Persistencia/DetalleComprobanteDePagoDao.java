package com.Maconsa.Persistencia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.DetalleComprobanteDePago;

@Repository
public interface DetalleComprobanteDePagoDao extends JpaRepository<DetalleComprobanteDePago, Integer>{

}
