package com.Maconsa.Persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Integer>{
	
	
	@Query(nativeQuery = true, value="select * from usuario group BY idusuario ASC  HAVING id_estado=1 limit ?1,10")
	public List<Usuario> listarUsuarios(int indice);
	
	@Query(nativeQuery = true, value="select count(*) from usuario where id_Estado=1")
	public int devolverTotalDeUsuarios();
	
	@Query(nativeQuery = true, value="SELECT * FROM usuario WHERE idusuario LIKE ?1 and id_estado=1")
	public List<Usuario> buscarUsuarioPorID(String descripcion);
	
	@Query(nativeQuery = true, value="SELECT * FROM usuario WHERE descripcion LIKE ?1 and id_estado=1")
	public List<Usuario> buscarUsuarioPorDescripcion(String descripcion);
	
	@Query(nativeQuery=true,value="select idusuario from usuario order by idusuario DESC limit 1")
	public int obtenerUltimoIDUsuario();


}
