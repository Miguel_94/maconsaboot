package com.Maconsa.Persistencia;

import java.awt.print.Pageable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.Maconsa.Entidad.Producto;

@Repository
public interface ProductoDao extends JpaRepository<Producto, Integer>{
	
	@Query(nativeQuery = true, value="select * from producto group BY id_Producto ASC  HAVING id_Estado=1 limit ?1,?2")
	public List<Producto> listaDeProductos(int indice,int tamanioPagina);
	
	@Query(nativeQuery = true, value="select * from producto group BY id_Producto ASC  HAVING id_Estado=1")
	public List<Producto> findAll();
	
	@Query(nativeQuery = true, value="select count(*) from producto where id_Estado=1")
	public int devolverTotalDeProductos();
	
	@Query(nativeQuery = true, value="SELECT * FROM producto WHERE id_producto LIKE ?1 and id_estado=1")
	public List<Producto> buscarProductoPorID(String descripcion);
	
	@Query(nativeQuery = true, value="SELECT * FROM producto WHERE descripcion LIKE ?1 and id_estado=1")
	public List<Producto> buscarProductoPorDescripcion(String descripcion);
	
	@Query(nativeQuery=true,value="select id_Producto from producto order by id_Producto DESC limit 1")
	public int obtenerUltimoIDProducto();
	
	
}
