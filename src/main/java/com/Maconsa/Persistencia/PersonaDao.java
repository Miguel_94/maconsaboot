package com.Maconsa.Persistencia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.Persona;
@Repository
public interface PersonaDao extends JpaRepository<Persona, String>{

}
