package com.Maconsa.Persistencia;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.ComprobanteDePago;

@Repository
public interface ComprobanteDePagoDao extends JpaRepository<ComprobanteDePago, Integer>{

	@Query(nativeQuery=true,value="select idcomprobantedepago from comprobantedepago order by idcomprobantedepago DESC limit 1")
	public double devolverUltimoIDComprobante();
}
