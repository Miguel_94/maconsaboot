package com.Maconsa.Persistencia;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.Maconsa.Entidad.Compra;
import com.Maconsa.Entidad.Kardex;
import com.Maconsa.Entidad.Producto;

@Repository
public interface InventarioDao extends JpaRepository<Kardex, Integer>{
	
	@Query(nativeQuery = true, value="select ROUND(sum(valor_venta*cantidad),2) from producto where id_Estado=1")
	public double devolverCostoInventario();
	
	@Query(nativeQuery = true, value="select c.id_compra from comprobantedepago cp inner join compra c on (c.idcomprobantedepago=cp.idcomprobantedepago) inner join tipoDeComprobante tcp on (tcp.idtipodecomprobantedepago=cp.idtipodecomprobantedepago) inner join detalleComprobanteDePago dcp on (dcp.idcomprobantedepago=cp.idcomprobantedepago) inner join producto p on(p.id_producto=dcp.idproducto) where year(c.fecha) =?1 and month(c.fecha) =?2 and p.id_producto=?3 and c.id_estado=1")
	public ArrayList<Integer> devolveridCompras(int año, int mes, int idProducto);
	
	@Query(nativeQuery = true, value="select v.id_venta from comprobantedepago cp inner join venta v on (v.idcomprobantedepago=cp.idcomprobantedepago) inner join tipoDeComprobante tcp on (tcp.idtipodecomprobantedepago=cp.idtipodecomprobantedepago) inner join detalleComprobanteDePago dcp on (dcp.idcomprobantedepago=cp.idcomprobantedepago) inner join producto p on(p.id_producto=dcp.idproducto) where year(v.fecha) =2019 and month(v.fecha) =10 and p.id_producto=1 and v.id_estado=1")
	public ArrayList<Integer> devolveridVentas();
	
	@Query(nativeQuery = true, value="SELECT stock_inicial from kardex where Month(fecha_inicio) =?1 and year(fecha_inicio) =?2 and id_producto=?3")
	public String devolverStockInicialProducto(int mes, int año, int idProducto);
}
