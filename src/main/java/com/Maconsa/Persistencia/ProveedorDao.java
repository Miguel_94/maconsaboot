package com.Maconsa.Persistencia;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.Maconsa.Entidad.Proveedor;

public interface ProveedorDao extends JpaRepository<Proveedor, String>{
	
	@Query(nativeQuery = true, value="select * from proveedor group BY ruc ASC  HAVING id_Estado=1 limit ?1,10")
	public List<Proveedor> listaProveedor(int indice);
	
	@Query(nativeQuery = true, value="select * from proveedor group BY ruc ASC  HAVING id_Estado=1")
	public List<Proveedor> findAll();
	
	@Query(nativeQuery = true, value="select count(*) from proveedor where id_Estado=1")
	 public int devolverTotalDeProveedor();
	
	@Query(nativeQuery = true, value="SELECT * FROM proveedor WHERE ruc LIKE ?1 and id_estado=1")
	public List<Proveedor> buscarProveedorPorRuc(String descripcion);
	
	@Query(nativeQuery = true, value="SELECT * FROM proveedor WHERE denominacion LIKE ?1 and id_estado=1")
	public List<Proveedor> buscarProveedorPorDescripcion(String descripcion);
	
}
