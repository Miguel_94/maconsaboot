package com.Maconsa.Persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionMysql {
private Connection conexion;
    
    public Connection getConexion()
    {
       return this.conexion;
    }

    
    public Connection  ConexionMysql(){
       try {
            //Class.forName("com.mysql.cj.jdbc.Driver");
            Class.forName("com.mysql.jdbc.Driver");
            this.conexion = DriverManager.getConnection("jdbc:mysql://localhost/dbprueba","root","");
           // this.conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/maconsa","root","root");
            return this.conexion;
         } catch (SQLException ex) {
            ex.printStackTrace();
         } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
         }
         return null;
    }
    
    
    //SET GLOBAL time_zone = '-3:00'; para configurar la hora en mysql Pacifico, Sudameric
}
